package com.cyberpro.lrm.paymentrouter;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.integration.support.MessageBuilder;

import com.cyberpro.lrm.paymentrouter.config.AppConfiguration;
import com.cyberpro.lrm.paymentrouter.dto.Payment;
import com.cyberpro.lrm.paymentrouter.dto.Payment.PaymentStatus;
import com.cyberpro.lrm.paymentrouter.dto.Payment.PaymentType;
import com.cyberpro.lrm.paymentrouter.gateway.IPaymentGateway;

@SpringBootApplication
public class PaymentApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfiguration.class);

		IPaymentGateway orderGateway = ctx.getBean(IPaymentGateway.class);

		getPaymentTransactionMap().forEach((transactionId, paymentList) -> orderGateway.processPaymentRequest(
				MessageBuilder.withPayload(paymentList).setHeader("TRANSACTION_ID", transactionId).build()));
	}

	/**
	 * Creates a sample payment transaction map covering multiple messages and
	 * returns.
	 *
	 * @return payment batch map
	 */
	private static Map<Integer, List<Payment>> getPaymentTransactionMap() {
		Map<Integer, List<Payment>> paymentTransactionMap = new HashMap<>();

		paymentTransactionMap.put(1, Arrays.asList(
				new Payment.PaymentBuilder("TRX1", "123", PaymentStatus.ACCEPTED, PaymentType.PAYMENT, 10.00).build()));

		return Collections.unmodifiableMap(paymentTransactionMap);
	}

}
