package com.cyberpro.lrm.paymentrouter.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.handler.annotation.Header;

import com.cyberpro.lrm.paymentrouter.dto.Payment;

@MessageEndpoint
public class DiscardedPaymentMessageListener {

	private final Logger logger = LoggerFactory.getLogger(DiscardedPaymentMessageListener.class);

	/**
	 * Handles discarded payment request(s) and logs.
	 *
	 * @param payment       message
	 * @param transactionId message header shows payment transaction id
	 */
	@ServiceActivator(inputChannel = "paymentFilterDiscardChannel")
	public void handleDiscardedPayment(Payment payment, @Header("TRANSACTION_ID") String transactionId) {
		logger.debug("Message in Transaction[" + transactionId + "] is received with Discarded payload : " + payment);
	}

}
