package com.cyberpro.lrm.paymentrouter.endpoint;

import java.util.List;

import org.springframework.integration.annotation.Aggregator;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.messaging.Message;

import com.cyberpro.lrm.paymentrouter.dto.Payment;

@MessageEndpoint
public class PaymentSplitter {

	/**
	 * Aggregates the Payment and Status to a Payment message
	 *
	 * @param message SI Message covering Payment List payload and Transaction Id
	 *                header.
	 * @return payment list
	 */
	@Aggregator(inputChannel = "paymentGWDefaultRequestChannel", outputChannel = "paymentSplitterOutputChannel")
	public List<Payment> splitPaymentList(Message<List<Payment>> message) {
		return message.getPayload();
	}
}
