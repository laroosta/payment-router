package com.cyberpro.lrm.paymentrouter.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Router;

import com.cyberpro.lrm.paymentrouter.dto.Payment;
import com.cyberpro.lrm.paymentrouter.service.DestinationService;

@MessageEndpoint
public class PaymentRouter {

	@Autowired
	DestinationService destinationService;

	/**
	 * Determines payment request's channel based on payment status.
	 *
	 * @param Payment message
	 * @return channel name
	 */
	@Router(inputChannel = "paymentFilterOutputChannel")
	public String route(Payment payment) {
		return destinationService.getDestination(payment);
	}

}
