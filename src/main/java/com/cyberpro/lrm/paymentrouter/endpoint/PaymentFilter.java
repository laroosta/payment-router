package com.cyberpro.lrm.paymentrouter.endpoint;

import org.springframework.integration.annotation.Filter;
import org.springframework.integration.annotation.MessageEndpoint;

import com.cyberpro.lrm.paymentrouter.dto.Payment;

@MessageEndpoint
public class PaymentFilter {

	/**
	 * Checks if Payment status is not rejected
	 *
	 * @param Payment message
	 * @return check result
	 */
	@Filter(inputChannel = "paymentSplitterOutputChannel", outputChannel = "paymentFilterOutputChannel", discardChannel = "paymentFilterDiscardChannel")
	public boolean filterIfPaymentNotRejected(Payment payment) {
		return payment.getPaymentStatus() != Payment.PaymentStatus.REJECTED;
	}
}