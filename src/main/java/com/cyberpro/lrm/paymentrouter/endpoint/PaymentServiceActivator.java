package com.cyberpro.lrm.paymentrouter.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.handler.annotation.Header;

import com.cyberpro.lrm.paymentrouter.dto.PaymentMessage;

@MessageEndpoint
public class PaymentServiceActivator {

	private final Logger logger = LoggerFactory.getLogger(PaymentServiceActivator.class);

	/**
	 * Gets processed requested and responded payment request(s) and logs.
	 *
	 * @param paymentMessage payment message
	 * @param transactionId  message header shows payment transaction id
	 */
	@ServiceActivator(inputChannel = "paymentTransformerOutputChannel")
	public void getPayment(PaymentMessage paymentMessage, @Header("TRANSACTION_ID") String transactionId) {
		logger.debug("Message with TransactionId [" + transactionId + "] is received with payload : " + paymentMessage);
		// Send Payment to Settlement Engine
	}

}