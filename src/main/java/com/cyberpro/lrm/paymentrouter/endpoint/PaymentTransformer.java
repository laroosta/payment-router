package com.cyberpro.lrm.paymentrouter.endpoint;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Transformer;

import com.cyberpro.lrm.paymentrouter.dto.Payment;
import com.cyberpro.lrm.paymentrouter.dto.SettlementRequestMessage;
import com.cyberpro.lrm.paymentrouter.dto.SettlementResponseMessage;

@MessageEndpoint
public class PaymentTransformer {

	/**
	 * Transforms Payment request to Settlement Request obj.
	 *
	 * @param payment request
	 * @return Settlement Request obj
	 */
	@Transformer(inputChannel = "paymentRouterSettlementRequestOutputChannel", outputChannel = "paymentTransformerOutputChannel")
	public SettlementRequestMessage transformSettlementRequestPayment(Payment payment) {
		return new SettlementRequestMessage(payment);
	}

	/**
	 * Transforms Payment request to Settlement Response obj.
	 *
	 * @param payment request
	 * @return Settlement Response obj
	 */
	@Transformer(inputChannel = "paymentRouterSettlementResponseOutputChannel", outputChannel = "paymentTransformerOutputChannel")
	public SettlementResponseMessage transformSettlementResponse(Payment payment) {
		return new SettlementResponseMessage(payment);
	}

}