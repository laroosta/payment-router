package com.cyberpro.lrm.paymentrouter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.messaging.MessageChannel;

@Configuration
@ComponentScan("com.cyberpro.lrm.paymentrouter")
@EnableIntegration
@IntegrationComponentScan("com.cyberpro.lrm.paymentrouter")
public class AppConfiguration {

	/**
	 * Creates a new paymentGWDefaultRequestChannel Channel and registers to
	 * BeanFactory.
	 *
	 * @return direct channel
	 */
	@Bean
	public MessageChannel paymentGWDefaultRequestChannel() {
		return new DirectChannel();
	}

	/**
	 * Creates a new paymentSplitterOutputChannel Channel and registers to
	 * BeanFactory.
	 *
	 * @return direct channel
	 */
	@Bean
	public MessageChannel paymentSplitterOutputChannel() {
		return new DirectChannel();
	}

	/**
	 * Creates a new paymentFilterOutput Channel and registers to BeanFactory.
	 *
	 * @return direct channel
	 */
	@Bean
	public MessageChannel paymentFilterOutputChannel() {
		return new DirectChannel();
	}

	/**
	 * Creates a new paymentRouterSettlementRequestOutput Channel and registers to
	 * BeanFactory.
	 *
	 * @return direct channel
	 */
	@Bean
	public MessageChannel paymentRouterSettlementRequestOutputChannel() {
		return new DirectChannel();
	}

	/**
	 * Creates a new paymentRouterSettlementResponseOutput Channel and registers to
	 * BeanFactory.
	 *
	 * @return direct channel
	 */
	@Bean
	public MessageChannel paymentRouterSettlementResponseOutputChannel() {
		return new DirectChannel();
	}

	/**
	 * Creates a new paymentTransformerOutput Channel and registers to BeanFactory.
	 *
	 * @return direct channel
	 */
	@Bean
	public MessageChannel paymentTransformerOutputChannel() {
		return new DirectChannel();
	}

}
