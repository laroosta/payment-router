package com.cyberpro.lrm.paymentrouter.service;

import org.springframework.stereotype.Service;

import com.cyberpro.lrm.paymentrouter.dto.Payment;
import com.cyberpro.lrm.paymentrouter.dto.Payment.PaymentStatus;

@Service
public class DestinationService {

	public String getDestination(Payment payment) {
		if (payment.getPaymentStatus() == PaymentStatus.ACCEPTED) {
			return "paymentRouterSettlementRequestOutputChannel";
		} else if (payment.getPaymentStatus() == PaymentStatus.COMPLETE) {
			return "paymentRouterSettlementResponseOutputChannel";
		}

		return "nullChannel";
	}
}
