package com.cyberpro.lrm.paymentrouter.dto;

public class SettlementRequestMessage extends PaymentMessage {

	public SettlementRequestMessage(Payment payment) {
		super(payment);
	}

	public String toString() {
		return "SettlementRequestMessage [payment=" + super.toString() + "]";
	}

}
