package com.cyberpro.lrm.paymentrouter.dto;

public class PaymentMessage {

	private final Payment payment;

	public PaymentMessage(Payment payment) {
		this.payment = payment;
	}

	public Payment getPayment() {
		return payment;
	}

	@Override
	public String toString() {
		return payment.toString();
	}
}