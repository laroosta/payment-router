package com.cyberpro.lrm.paymentrouter.dto;

public class Payment {

	public enum PaymentType {
		PAYMENT, TRANSFER
	}

	public enum PaymentStatus {
		REJECTED, ACCEPTED, PENDING, COMPLETE, FAILED
	}

	private final String transactionId;
	private final String accountNumber;
	private final PaymentStatus paymentStatus;
	private final PaymentType paymentType;
	private final double amount;

	private Payment(PaymentBuilder paymentBuilder) {
		this.transactionId = paymentBuilder.transactionId;
		this.accountNumber = paymentBuilder.accountNumber;
		this.paymentStatus = paymentBuilder.paymentStatus;
		this.paymentType = paymentBuilder.paymentType;
		this.amount = paymentBuilder.amount;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public double getAmount() {
		return amount;
	}

	@Override
	public String toString() {
		return "Payment [transactionId=" + transactionId + ", paymentStatus=" + paymentStatus + ", paymentType="
				+ paymentType + ", accountNumber=" + amount + "]";
	}

	public static class PaymentBuilder {

		private final String transactionId;
		private final String accountNumber;
		private PaymentStatus paymentStatus;
		private PaymentType paymentType;
		private double amount;

		public PaymentBuilder(String transationId, String accountNumber, PaymentStatus paymentStatus,
				PaymentType paymentType, double amount) {
			this.transactionId = transationId;
			this.accountNumber = accountNumber;
			this.paymentStatus = paymentStatus;
			this.paymentType = paymentType;
			this.amount = amount;
		}

		public PaymentBuilder setStatus(PaymentStatus paymentStatus) {
			this.paymentStatus = paymentStatus;
			return this;
		}

		public Payment build() {
			Payment payment = new Payment(this);
			if (PaymentType.TRANSFER == payment.getPaymentType()) {
				throw new IllegalStateException(
						"Type is invalid! Payment Transaction Id : " + payment.getTransactionId());
			}

			return payment;
		}

	}
}