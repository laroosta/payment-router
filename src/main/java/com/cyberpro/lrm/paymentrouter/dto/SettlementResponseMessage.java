package com.cyberpro.lrm.paymentrouter.dto;

public class SettlementResponseMessage extends PaymentMessage {

	public SettlementResponseMessage(Payment payment) {
		super(payment);
	}

	@Override
	public String toString() {
		return "SettlementResponseMessage [payment=" + super.toString() + "]";
	}

}