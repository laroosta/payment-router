package com.cyberpro.lrm.paymentrouter.gateway;

import java.util.List;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.Message;

import com.cyberpro.lrm.paymentrouter.dto.Payment;

@MessagingGateway(name = "paymentGateway", defaultRequestChannel = "paymentGWDefaultRequestChannel")
public interface IPaymentGateway {

	/**
	 * Processes Payment Request
	 *
	 * @param message SI Message covering Payment List payload and Transaction Id
	 *                header.
	 * @return operation result
	 */
	@Gateway
	void processPaymentRequest(Message<List<Payment>> message);
}